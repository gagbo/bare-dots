#!/usr/bin/env sh
killall -9 sxhkd redshift dunst compton nm-applet xfce4-power-manager xss-lock

# Compositing
compton --conf "${HOME}/.config/compton.conf" &

# Screen locking
xss-lock -n "${HOME}/bin/dim-screen.sh" -- "${HOME}/bin/i3lock-laptop.sh" &

# Wallpaper
# shellcheck source=/dev/null
[ -r "${HOME}/.fehbg" ] && . "${HOME}/.fehbg"

# Keybindings
sxhkd &

# Notifications
dunst &

# Bar
"${HOME}/.config/polybar/launch.sh"

# Tray apps
xfce4-power-manager &
nm-applet --sm-disable &
