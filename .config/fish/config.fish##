# Add virtualfish for environment
eval (python3 -m virtualfish)

# Emacs keybindings
# The vi keybindings trigger redrawing which is buggy in alacritty :(
fish_default_key_bindings

# Recreate aliases

# Misc random
alias starwars6 'telnet -6 towel.blinkenlights.nl'
alias starwars 'telnet towel.blinkenlights.nl'

# Tmux stuff
alias tmk 'tmux -u kill-session -t'
alias attach 'tmux -u attach -t'
alias tmux 'tmux -u'

# Quick terminals
alias ki 'kitty'
alias ur 'urxvt256c-ml'
alias ko 'konsole'
alias xf 'xfce4-terminal'

# Common ssh targets
alias sb 'ssh gagbo@Bulbizarre'
alias sf 'ssh gerry.agbobada@front-in1'

# Common commands
alias vi 'vimx'
alias nv 'nvim'
alias em 'emacsclient -nw -c -a ""'
alias ge 'emacsclient -n -c -a ""'
alias ok 'okular'

# Git shortcuts
alias gll 'git log --oneline --all --graph --decorate'
alias gl 'git log --oneline --graph --decorate'

# Misc magic
alias loc 'tokei'
alias dr 'disown -r'

# Folder information
alias lll 'exa -laFhS'
alias ll 'exa -l'
alias la 'exa -la'
alias tree 'exa -T'

# Common folders
alias .. 'cd ..'
alias .p 'cd ~/projects'
alias .e 'cd ~/devel/ensmp'
alias .s 'cd ~/stow'
alias .v 'cd ~/.config/nvim'

# Git Prompt settings
set -g __fish_git_prompt_show_informative_status 1
set -g __fish_git_prompt_hide_untrackedfiles 1

set -g __fish_git_prompt_color_branch magenta --bold
set -g __fish_git_prompt_showupstream "informative"
set -g __fish_git_prompt_char_upstream_ahead "↑"
set -g __fish_git_prompt_char_upstream_behind "↓"
set -g __fish_git_prompt_char_upstream_prefix ""

set -g __fish_git_prompt_char_stagedstate "●"
set -g __fish_git_prompt_char_dirtystate "✚"
set -g __fish_git_prompt_char_untrackedfiles "…"
set -g __fish_git_prompt_char_conflictedstate "✖"
set -g __fish_git_prompt_char_cleanstate "✔"

set -g __fish_git_prompt_color_dirtystate blue
set -g __fish_git_prompt_color_stagedstate yellow
set -g __fish_git_prompt_color_invalidstate red
set -g __fish_git_prompt_color_untrackedfiles $fish_color_normal
set -g __fish_git_prompt_color_cleanstate green --bold

if test -r ~/.config/fish/local_config.fish
   source ~/.config/fish/local_config.fish
end
