function fish_prompt
    # $status gets nuked as soon as something else is run, e.g. set_color
    # so it has to be saved asap.
    set -l last_status $status

    _set_colors

    # Clear the line because fish seems to emit the prompt twice. The initial
    # display, then when you press enter.
    printf "\033[K"

    _date_section
    printf " "
    _last_status_section $last_status
    printf " "
    _disk_usage_section
    printf " "
    _current_dir_section
    printf "\n"
    _venv_section
    _git_section
    printf "$cu> "
end

function _last_status_section
    if [ $argv[1] -ne 0 ]
	printf "$cr$cbb:$cr $argv[1]"
    else
	printf "$cg✔"
    end
end

function _date_section
    printf (date "+$cy%H$cbb:$cy%M$cbb:$cy%S")
end

function _disk_usage_section
    # Show disk usage when low
    set -l du (df / | tail -n1 | sed "s/  */ /g" | cut -d' ' -f 5 | cut -d'%' -f1)
    if test $du -gt 80
	printf "$crdu$cbb:$cr$du%%"
    end
end

function _current_dir_section
    # Current Directory
    # 1st sed for colourising forward slashes
    # 2nd sed for colourising the deepest path (the 'm' is the last char in the
    # ANSI colour code that needs to be stripped)
    printf (pwd | sed "s,/,$cg/$cg,g" | sed "s,\(.*\)/[^m]*m,\1/$cbg,")
end

function _venv_section
    # Virtual Env
    if set -q VIRTUAL_ENV
	printf "$cu(venv: $cw"
	printf (basename "$VIRTUAL_ENV")
	printf "$cu)"
    end
end

function _git_section
    __fish_git_prompt
end

function _set_colors
    set -g cb (set_color black)
    set -g cr (set_color red)
    set -g cy (set_color yellow)
    set -g cg (set_color green)
    set -g cu (set_color blue)
    set -g cm (set_color magenta)
    set -g cc (set_color cyan)
    set -g cw (set_color white)
    set -g cbb (set_color brblack)
    set -g cbr (set_color brred)
    set -g cby (set_color bryellow)
    set -g cbg (set_color brgreen)
    set -g cbu (set_color brblue)
    set -g cbm (set_color brmagenta)
    set -g cbc (set_color brcyan)
    set -g cbw (set_color brwhite)
end
