# i3 config file (v4)
# Please see https://i3wm.org/docs/userguide.html for a complete reference!
# vim: fdm=marker :

# Init {{{1
    set $mod Mod4

    font pango:KoHo Medium 10
    # Backup
    # font pango:DejaVu Sans Mono 8

    # set $menu dmenu_path | dmenu | xargs swaymsg exec
    set $menu "krunner"
    set $locker "xset s activate"
    set $term kitty

    # Startup programs {{{2
        exec_always --no-startup-id ${HOME}/.fehbg
        #exec_always --no-startup-id feh --bg-scale ${HOME}/Images/wallpapers/background
    # Bar {{{2
        # bar {
        #     font pango:Noto Sans 10
        #     status_command i3blocks
        #     position top
        #     separator_symbol "|"
        # }

# i3 session control {{{1
    # reload the configuration file
    bindsym $mod+q reload
    # restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
    bindsym $mod+Shift+r restart
    # exit i3 (logs you out of your X session)
    bindsym $mod+Shift+q exec "~/bin/dmenu/prompt 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' 'i3-msg exit'"

# Basic commands{{{1
    # Use Mouse+$mod to drag floating windows to their wanted position
    floating_modifier $mod

    # start a terminal
    # bindsym $mod+Return exec i3-sensible-terminal
    bindsym $mod+Return exec $term

    # kill focused window
    bindsym $mod+c kill

    # start dmenu (a program launcher)
    bindsym $mod+d exec --no-startup-id $menu

    # lock the computer
    bindsym Control+$mod+l exec $locker

# Navigation {{{1
    # change focus
    bindsym $mod+h focus left
    bindsym $mod+j focus down
    bindsym $mod+k focus up
    bindsym $mod+l focus right

    # alternatively, you can use the cursor keys:
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # move focused window
    bindsym $mod+Shift+h move left
    bindsym $mod+Shift+j move down
    bindsym $mod+Shift+k move up
    bindsym $mod+Shift+l move right

    # alternatively, you can use the cursor keys:
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right

    # change focus between tiling / floating windows
    bindsym $mod+space focus mode_toggle

    # focus the parent container
    bindsym $mod+a focus parent

    # focus the child container
    bindsym $mod+z focus child

    focus_on_window_activation none
    focus_follows_mouse no
    mouse_warping none

# Layout {{{1
    # split in horizontal orientation
    bindsym $mod+b split h

    # split in vertical orientation
    bindsym $mod+v split v

    # enter fullscreen mode for the focused container
    bindsym $mod+f fullscreen toggle

    # change container layout (stacked, tabbed, toggle split)
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # toggle tiling / floating
    bindsym $mod+Shift+space floating toggle

# Workspaces {{{1
    # Define names for default workspaces for which we configure key bindings later on.
    # We use variables to avoid repeating the names in multiple places.
    set $ws1 1
    set $ws2 2
    set $ws3 3
    set $ws4 4
    set $ws5 5
    set $ws6 6
    set $ws7 7
    set $ws8 8

    # switch to workspace
    bindsym $mod+1      workspace $ws1
    bindsym Control+F1  workspace $ws1
    bindsym $mod+2      workspace $ws2
    bindsym Control+F2  workspace $ws2
    bindsym $mod+3      workspace $ws3
    bindsym Control+F3  workspace $ws3
    bindsym $mod+4      workspace $ws4
    bindsym Control+F4  workspace $ws4
    bindsym $mod+5      workspace $ws5
    bindsym Control+F5  workspace $ws5
    bindsym $mod+6      workspace $ws6
    bindsym Control+F6  workspace $ws6
    bindsym $mod+7      workspace $ws7
    bindsym Control+F7  workspace $ws7
    bindsym $mod+8      workspace $ws8
    bindsym Control+F8  workspace $ws8

    # move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace $ws1
    bindsym $mod+Shift+2 move container to workspace $ws2
    bindsym $mod+Shift+3 move container to workspace $ws3
    bindsym $mod+Shift+4 move container to workspace $ws4
    bindsym $mod+Shift+5 move container to workspace $ws5
    bindsym $mod+Shift+6 move container to workspace $ws6
    bindsym $mod+Shift+7 move container to workspace $ws7
    bindsym $mod+Shift+8 move container to workspace $ws8

# Resizing / Gaps {{{1
    # resize window (you can also use the mouse for that)
    mode "resize" {
            # These bindings trigger as soon as you enter the resize mode

            # Pressing left will shrink the window’s width.
            # Pressing right will grow the window’s width.
            # Pressing up will shrink the window’s height.
            # Pressing down will grow the window’s height.
            bindsym h resize shrink width 10 px or 10 ppt
            bindsym j resize grow height 10 px or 10 ppt
            bindsym k resize shrink height 10 px or 10 ppt
            bindsym l resize grow width 10 px or 10 ppt

            # same bindings, but for the arrow keys
            bindsym Left resize shrink width 10 px or 10 ppt
            bindsym Down resize grow height 10 px or 10 ppt
            bindsym Up resize shrink height 10 px or 10 ppt
            bindsym Right resize grow width 10 px or 10 ppt

            # back to normal: Enter or Escape or $mod+r
            bindsym Return mode "default"
            bindsym Escape mode "default"
            bindsym $mod+r mode "default"
    }

    bindsym $mod+r mode "resize"
    # Gaps {{{2
        # Set inner/outer gaps
        gaps inner 10
        gaps outer -4

        # Additionally, you can issue commands with the following syntax. This is useful to bind keys to changing the gap size.
        # gaps inner|outer current|all set|plus|minus <px>
        # gaps inner all set 10
        # gaps outer all plus 5

        # Smart gaps (gaps used if only more than one container on the workspace)
        smart_gaps on

        # Smart borders (draw borders around container only if it is not the only container on this workspace)
        # on|no_gaps (on=always activate and no_gaps=only activate if the gap size to the edge of the screen is 0)
        smart_borders on

        # Press $mod+Shift+g to enter the gap mode.
        # Choose o or i for modifying outer/inner gaps.
        # Press one of + / - (in-/decrement for current workspace)
        # or 0 (remove gaps for current workspace). If you also press Shift
        # with these keys, the change will be global for all workspaces.
        set $mode_gaps Gaps: (o) outer, (i) inner
        set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
        set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
        bindsym $mod+Shift+g mode "$mode_gaps"

        mode "$mode_gaps" {
                bindsym o      mode "$mode_gaps_outer"
                bindsym i      mode "$mode_gaps_inner"
                bindsym Return mode "default"
                bindsym Escape mode "default"
        }
        mode "$mode_gaps_inner" {
                bindsym plus  gaps inner current plus 5
                bindsym minus gaps inner current minus 5
                bindsym 0     gaps inner current set 0

                bindsym Shift+plus  gaps inner all plus 5
                bindsym Shift+minus gaps inner all minus 5
                bindsym Shift+0     gaps inner all set 0

                bindsym Return mode "default"
                bindsym Escape mode "default"
        }
        mode "$mode_gaps_outer" {
                bindsym plus  gaps outer current plus 5
                bindsym minus gaps outer current minus 5
                bindsym 0     gaps outer current set 0

                bindsym Shift+plus  gaps outer all plus 5
                bindsym Shift+minus gaps outer all minus 5
                bindsym Shift+0     gaps outer all set 0

                bindsym Return mode "default"
                bindsym Escape mode "default"
        }


# Colors {{{1
    set_from_resource $black       i3wm.color0  #1C1B19
    set_from_resource $int_black   i3wm.color8  #2D2C29

    set_from_resource $red         i3wm.color1  #EF2F27
    set_from_resource $int_red     i3wm.color9  #F75341

    set_from_resource $green       i3wm.color2  #519F50
    set_from_resource $int_green   i3wm.color10 #98BC37

    set_from_resource $yellow      i3wm.color3  #FBB829
    set_from_resource $int_yellow  i3wm.color11 #FED06E

    set_from_resource $blue        i3wm.color4  #2C78BF
    set_from_resource $int_blue    i3wm.color12 #68A8E4

    set_from_resource $magenta     i3wm.color5  #E02C6D
    set_from_resource $int_magenta i3wm.color13 #FF5C8F

    set_from_resource $cyan        i3wm.color6  #0AAEB3
    set_from_resource $int_cyan    i3wm.color14 #53FDE9

    set_from_resource $white       i3wm.color7  #918175
    set_from_resource $int_white   i3wm.color15 #FCE8C3

    # client.focused
    #     A client which currently has the focus.
    # client.focused_inactive
    #     A client which is the focused one of its container,
    #     but it does not have the focus at the moment.
    # client.unfocused
    #     A client which is not the focused one of its container.
    # client.urgent
    #     A client which has its urgency hint activated.
    # client.placeholder
    #     Background and text color are used to draw placeholder
    #     window contents (when restoring layouts).
    #     Border and indicator are ignored.
    # client.background
    #     Background color which will be used to paint the
    #     background of the client window on top of which the
    #     client will be rendered. Only clients which do not
    #     cover the whole area of this window expose the color.
    #     Note that this colorclass only takes a single color.

    #class                  border     backgr.     text          indicator   child_border
    client.focused          $blue      $blue       $int_white    $yellow     $blue
    client.focused_inactive $blue      $int_black  $white        $yellow     $black
    client.unfocused        $white     $int_black  $white        $yellow     $int_black
    client.urgent           $red       $int_black  $int_white    $yellow     $black
    client.placeholder      $int_black $int_black  $int_white    $yellow     $black

    client.background       $black


# Shutdown / Logout menu {{{1
    set $mode_system System (l) lock, (e) logout, (s) suspend, (r) reboot, (Shift+s) shutdown
    mode "$mode_system" {
        bindsym l exec --no-startup-id $locker, mode "default"
        bindsym e exec --no-startup-id i3-msg exit, mode "default"
        bindsym s exec --no-startup-id systemctl suspend, mode "default"
        bindsym r exec --no-startup-id systemctl reboot, mode "default"
        bindsym Shift+s exec --no-startup-id systemctl poweroff -i, mode "default"

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
    }

    bindsym $mod+Delete mode "$mode_system"

# Media / XF86 keys {{{1
    # Volume {{{2
        set $vol_increment 4%
        bindsym XF86AudioLowerVolume exec amixer -q -D pulse sset Master $vol_increment- && pkill -RTMIN+10 i3blocks
        bindsym XF86AudioRaiseVolume exec amixer -q -D pulse sset Master $vol_increment+ && pkill -RTMIN+10 i3blocks
        bindsym XF86AudioMute        exec amixer -q -D pulse sset Master toggle && pkill -RTMIN+10 i3blocks

    # Play control {{{2
        bindsym XF86AudioPlay   exec --no-startup-id mpc -q toggle
        bindsym XF86AudioPause  exec --no-startup-id mpc -q toggle
        bindsym XF86AudioToggle exec --no-startup-id mpc -q toggle
        bindsym XF86AudioStop   exec --no-startup-id mpc -q stop
        bindsym XF86AudioPrev   exec --no-startup-id mpc -q prev
        bindsym XF86AudioNext   exec --no-startup-id mpc -q next

    # Brightness {{{2
        set $bright_increment 4%
        bindsym XF86MonBrightnessDown exec brightnessctl -d intel_backlight set $bright_increment-
        bindsym XF86MonBrightnessUp   exec brightnessctl -d intel_backlight set $bright_increment+

# Quick floating applications {{{1
    bindsym $mod+m exec $term --class MPCWindow -e ncmpcpp
    bindsym $mod+i exec $term --class HTOPWindow -e htop
    # TODO : put weechat in the scroll view thingy
    bindsym $mod+c exec $term --class IRCWindow -e weechat

# Plasma compat {{{1
    for_window [window_role="pop-up"] floating enable
    for_window [class="plasmashell"] floating enable

    for_window [title="Desktop — Plasma"] kill;
    for_window [title="Bureau — Plasma"] kill;

    no_focus [class="plasmashell"]
    no_focus [window_role="pop-up"]
    no_focus [window_type="notification"]

# Floating windows {{{1
    for_window [class="Matplotlib"] floating enable
    for_window [class="ParaView"] floating enable
    for_window [class="HTOPWindow"] floating enable
    for_window [class="MPCWindow"] floating enable
