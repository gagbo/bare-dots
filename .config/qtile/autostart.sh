#!/usr/bin/env sh
killall -9 redshift dunst compton nm-applet xfce4-power-manager xss-lock keepassxc blueman-manager
pulseaudio --check && pulseaudio -k

# Get network names
"${HOME}/bin/export_network_names.sh"
export ETH_NAME
export WIRELESS_NAME

# Compositing
compton --conf "${HOME}/.config/compton.conf" &

# Screen locking
xss-lock -n "${HOME}/bin/dim-screen.sh" -- "${HOME}/bin/i3lock-laptop.sh" &

# Wallpaper
# shellcheck source=/dev/null
[ -r "${HOME}/.fehbg" ] && . "${HOME}/.fehbg"

# Notifications
dunst &

# Tray apps
xfce4-power-manager &
nm-applet --sm-disable &

owncloud &
keepassxc &
blueman-manager &
