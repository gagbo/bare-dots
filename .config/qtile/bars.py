import os
import subprocess

from libqtile import bar, widget

# Colors from Gorgehousse colorscheme
FOREGROUND = "CEDBE5"
BACKGROUND = "0C0C14"
SELECTION = "247AB7"
BLACK = "0D0D16"
RED = "B74A40"
GREEN = "99B740"
YELLOW = "B7892D"
BLUE = "2461B7"
MAGENTA = "672DB7"
CYAN = "2DACB7"
WHITE = "899299"
BRIGHT_BLACK = "2D2D4C"
BRIGHT_RED = "E55C50"
BRIGHT_GREEN = "C0E550"
BRIGHT_YELLOW = "E5AC39"
BRIGHT_BLUE = "2D7AE5"
BRIGHT_MAGENTA = "8139E5"
BRIGHT_CYAN = "39D7E5"
BRIGHT_WHITE = "E5F4FF"
ORANGE = "D75F00"
BRIGHT_ORANGE = "FF8700"
DARKER_GRAY = "1E1E33"

DARK_BAR_BG = ["0C0C14", "0D0D16", "1E1E33", "2D2D4C"]

widget_defaults = dict(
    font="KoHo Medium",
    fontsize=16,
    padding=3,
    foreground=FOREGROUND,
    background=BACKGROUND,
)

home = os.path.expanduser("~")
local_scripts = home + "/bin/"


def get_weather():
    """ Return the weather as formatted in the openweathermap script """
    return (
        subprocess.check_output(
            [local_scripts + "openweathermap-fullfeatured-with-key.sh"]
        )
        .decode("utf-8")
        .strip()
    )


def get_battery_info():
    """ Return the battery info """
    return (
        subprocess.check_output([local_scripts + "battery-combined-shell.sh"])
        .decode("utf-8")
        .strip()
    )


def get_kb_layout():
    """ Return the current keyboard layout """
    try:
        layout = subprocess.check_output(["xkb-switch"]).decode("utf-8").strip()
        return layout
    except Exception:
        return ""


# Bash equivalent : cat /proc/acpi/ibm/fan | grep speed: | cut -f 3
def get_fan_speed(fan_file_path="/proc/acpi/ibm/fan"):
    """ Return the fan speed read from fan_file_path.
        Expects a line like 'speed:  XXX' where XXX is the fan speed """
    try:
        with open(fan_file_path, "r") as info:
            for line in info:
                if not line.startswith("speed:"):
                    continue
                speed = line.split()[1]
                return str(speed) + " RPM"
            return "N/A"
    except Exception:
        return "N/A"


def powerline_separator(left_col, right_col, left_to_right=True):
    symbol = ""
    back_col = left_col
    fore_col = right_col
    if left_to_right:
        symbol = ""
        back_col = right_col
        fore_col = left_col
    return widget.TextBox(
        symbol, background=back_col, foreground=fore_col, padding=0, fontsize=30
    )


top_bar = bar.Bar(
    [
        widget.GroupBox(
            foreground=WHITE,
            highlight_method="block",
            urgent_alert_method="block",
            active=FOREGROUND,
            urgent_border=RED,
            urgent_text=RED,
            this_screen_border=BLUE,
            this_current_screen_border=BLUE,
            other_screen_border=BLUE,
            other_current_screen_border=BLUE,
            border_width=2,
        ),
        widget.Prompt(),
        powerline_separator(BACKGROUND, DARKER_GRAY),
        widget.CurrentLayout(foreground=WHITE, background=DARKER_GRAY),
        powerline_separator(DARKER_GRAY, DARK_BAR_BG[0]),
        widget.WindowName(background=DARK_BAR_BG[0], foreground=FOREGROUND),
        powerline_separator(DARK_BAR_BG[0], DARK_BAR_BG[1], False),
        widget.Clock(
            format="%A %d %b %Y %H:%M:%S",
            foreground=FOREGROUND,
            background=DARK_BAR_BG[1],
        ),
        powerline_separator(DARK_BAR_BG[1], DARK_BAR_BG[2], False),
        widget.GenPollText(
            func=get_weather,
            update_interval=60,
            font="Weather Icons",
            background=DARK_BAR_BG[2],
            foreground=FOREGROUND,
        ),
        powerline_separator(DARK_BAR_BG[2], DARK_BAR_BG[3], False),
        widget.ThermalSensor(
            tag_sensor="Package id 0",
            update_interval=2,
            background=DARK_BAR_BG[3],
            foreground=FOREGROUND,
        ),
        widget.GenPollText(
            func=get_fan_speed,
            update_interval=2,
            background=DARK_BAR_BG[3],
            foreground=FOREGROUND,
        ),
        widget.TextBox("CPU", background=DARK_BAR_BG[3], foreground=FOREGROUND),
        widget.CPUGraph(
            background=DARK_BAR_BG[3],
            graph_color=FOREGROUND,
            fill_color=WHITE + ".3",
            line_width=1,
            border_width=1,
            border_color=BLACK,
        ),
        widget.TextBox("MEM", background=DARK_BAR_BG[3], foreground=FOREGROUND),
        widget.MemoryGraph(
            background=DARK_BAR_BG[3],
            graph_color=FOREGROUND,
            fill_color=WHITE + ".3",
            line_width=1,
            border_width=1,
            border_color=BLACK,
        ),
    ],
    35,
)

bottom_bar = bar.Bar(
    [
        widget.TextBox("☼", background=DARK_BAR_BG[3], foreground=FOREGROUND),
        widget.Backlight(
            backlight_name="intel_backlight",
            step=4,
            background=DARK_BAR_BG[3],
            foreground=FOREGROUND,
        ),
        widget.GenPollText(
            func=get_battery_info,
            update_interval=60,
            background=DARK_BAR_BG[3],
            foreground=FOREGROUND,
        ),
        powerline_separator(DARK_BAR_BG[3], DARK_BAR_BG[2]),
        widget.GenPollText(
            func=get_kb_layout,
            update_interval=1,
            background=DARK_BAR_BG[2],
            foreground=WHITE,
        ),
        widget.CapsNumLockIndicator(background=DARK_BAR_BG[2], foreground=WHITE),
        powerline_separator(DARK_BAR_BG[2], DARK_BAR_BG[1]),
        widget.Mpd2(
            status_format="{play_status} {elapsed}/{duration} "
            + "{artist} - {title} "
            + "[{repeat}{random}{single}{consume}{updating_db}]",
            background=DARK_BAR_BG[1],
            foreground=FOREGROUND,
        ),
        powerline_separator(DARK_BAR_BG[1], DARK_BAR_BG[0]),
        widget.TextBox("🔉", background=DARK_BAR_BG[0], foreground=FOREGROUND),
        widget.Volume(background=DARK_BAR_BG[0], foreground=FOREGROUND),
        powerline_separator(DARK_BAR_BG[0], BACKGROUND),
        widget.Spacer(),
        powerline_separator(BACKGROUND, DARK_BAR_BG[2], False),
        widget.Wlan(
            disconnected_message="",
            interface=os.getenv("WIRELESS_NAME", "wlan0"),
            background=DARK_BAR_BG[2],
            foreground=FOREGROUND,
        ),
        powerline_separator(DARK_BAR_BG[2], DARK_BAR_BG[1], False),
        widget.TextBox("NET", background=DARK_BAR_BG[1], foreground=FOREGROUND),
        widget.Net(
            update_interval=1,
            disconnected_message="",
            interface=os.getenv("WIRELESS_NAME", "wlan0"),
            background=DARK_BAR_BG[1],
            foreground=FOREGROUND,
        ),
        widget.NetGraph(
            interface=os.getenv("WIRELESS_NAME", "wlan0"),
            background=DARK_BAR_BG[1],
            graph_color=FOREGROUND,
            fill_color=WHITE + ".3",
            line_width=1,
            border_width=1,
            border_color=BLACK,
        ),
        widget.Systray(),
    ],
    35,
)
