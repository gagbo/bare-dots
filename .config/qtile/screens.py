from libqtile.config import Screen

from bars import top_bar, bottom_bar

main_screen = Screen(top=top_bar, bottom=bottom_bar)
screens = [main_screen]
