import subprocess

# pylint: disable=C0111
c = c  # noqa: F821 pylint: disable=E0602,C0103
config = config  # noqa: F821 pylint: disable=E0602,C0103

config.load_autoconfig()


def read_xresources(prefix):
    props = {}
    x = subprocess.run(["xrdb", "-query"], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split("\n")
    for line in filter(lambda l: l.startswith(prefix), lines):
        prop, _, value = line.partition(":\t")
        props[prop] = value
    return props


xresources = read_xresources("*")
config.source("nord-qutebrowser.py")
c.colors.statusbar.normal.bg = xresources["*.background"]

config.bind(';b', 'hint all tab-bg', mode='normal')
config.bind(';f', 'hint all tab-fg', mode='normal')
config.bind(';h', 'hint all hover', mode='normal')
config.bind(';i', 'hint images', mode='normal')
config.bind(';I', 'hint images tab', mode='normal')
config.bind(';o', 'hint links fill :open {hint-url}', mode='normal')
config.bind(';O', 'hint links fill :open -t -r {hint-url}', mode='normal')
config.bind(';y', 'hint links yank', mode='normal')
config.bind(';Y', 'hint links yank-primary', mode='normal')
config.bind(';r', 'hint --rapid links tab-bg', mode='normal')
config.bind(';R', 'hint --rapid links window', mode='normal')
config.bind(';d', 'hint links download', mode='normal')
config.bind(';t', 'hint inputs', mode='normal')

# Youtube video hints
config.bind(';M', 'hint --rapid links spawn vlc {hint-url}')
config.bind('M', 'hint links spawn vlc {hint-url}')

# Search
## General
c.url.searchengines['DEFAULT'] = 'https://qwant.com/?q={}'
c.url.searchengines['d'] = 'https://duckduckgo.com/?q={}'
c.url.searchengines['g'] = 'http://www.google.com/search?hl=en&source=hp&ie=ISO-8859-l&q={}'

## Videos
c.url.searchengines['y'] = 'https://www.youtube.com/results?search_query={}'

## Wiki
c.url.searchengines['w'] = 'https://secure.wikimedia.org/wikipedia/en/w/index.php?title=Special%%3ASearch&search={}'
c.url.searchengines['wfr'] = 'https://secure.wikimedia.org/wikipedia/fr/w/index.php?title=Special%%3ASearch&search={}'

## Code
c.url.searchengines['a'] = 'https://wiki.archlinux.org/?search={}'
c.url.searchengines['gh'] = 'https://github.com/search?q={}&type=Code'
c.url.searchengines['cppref'] = 'https://en.cppreference.com/mwiki/index.php?title=Special%3ASearch&search={}'
