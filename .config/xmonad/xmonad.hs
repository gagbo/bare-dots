{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

--------------------------------------------------------------------------------
-- | Example.hs
--
-- Example configuration file for xmonad using the latest recommended
-- features (e.g., 'desktopConfig').
module Main (main) where

--------------------------------------------------------------------------------
import           Control.Monad
import           System.Exit
import           XMonad
import           XMonad.Actions.GridSelect
import           XMonad.Actions.Volume
import           XMonad.Actions.Promote
import           XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import           XMonad.Config.Desktop
import           XMonad.Hooks.DynamicLog
import           XMonad.Hooks.EwmhDesktops
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers
import           XMonad.Hooks.UrgencyHook
import           XMonad.Layout.BinarySpacePartition (emptyBSP)
import           XMonad.Layout.Gaps
import           XMonad.Layout.Grid
import           XMonad.Layout.LayoutModifier
import           XMonad.Layout.NoBorders            (noBorders, smartBorders)
import           XMonad.Layout.ResizableTile        (ResizableTall (..))
import           XMonad.Layout.Tabbed
import           XMonad.Layout.ToggleLayouts        (ToggleLayout (..),
                                                     toggleLayouts)
import           XMonad.Prompt
import           XMonad.Prompt.ConfirmPrompt
import           XMonad.Prompt.Shell
import qualified XMonad.StackSet as W
import           XMonad.Util.EZConfig
import           XMonad.Util.Run
import           XMonad.Util.SpawnNamedPipe
import           XMonad.Util.SpawnOnce

--------------------------------------------------------------------------------
-- | Start xmonad using the main desktop configuration with
-- my overrides:
main :: IO ()
main = xmonad $ withUrgencyHook NoUrgencyHook
              $ ewmh $ docks desktopConfig
    { terminal   = myTerminal
    , modMask    = myModKey
    , normalBorderColor = "#899299"
    , focusedBorderColor = "#2461B7"
    , borderWidth = 2
    , startupHook = myStartupHook
    , handleEventHook = myEventHook <+> handleEventHook desktopConfig
    , manageHook = myManageHook
    , logHook    = myLogHook "xmobtop" <+> logHook desktopConfig
    , layoutHook = smartBorders . avoidStruts $ myOuterGaps myLayouts
    }
    `additionalKeysP` myKeyBindings

--------------------------------------------------------------------------------
-- | Customize Startup
--
myStartupHook :: X ()
myStartupHook = do
    spawnOnce "dunst"
    spawnNamedPipe "xmobar ~/.config/xmobar/xmobarrc-top" "xmobtop"
    spawnOnce "stalonetray -c ~/.config/stalonetray/stalonetrayrc"
    spawnOnce "xmobar ~/.config/xmobar/xmobarrc-bottom"
    spawnOnce "owncloud-client"

--------------------------------------------------------------------------------
-- | Customize the PrettyPrinting in Xmobar status bar
myLogHook :: String -> X ()
myLogHook pipeName = do
    mh <- getNamedPipe pipeName
    dynamicLogWithPP myXMobarPP {
      ppOutput = maybe (\_ -> return ()) hPutStrLn mh
    }

myXMobarPP :: PP
myXMobarPP = xmobarPP {
          ppTitle = xmobarColor "#2DACB7" "" . shorten 50
          , ppCurrent = xmobarColor "#E5AC39" "" . wrap "[" "]"
          , ppHiddenNoWindows = xmobarColor "#2D2D4C" ""
          , ppUrgent = xmobarColor "#E55C50" "#E5F4FF"
        }

--------------------------------------------------------------------------------
-- | Customize Events
--
myEventHook = fullscreenEventHook

------------------------------------------------------------------------
---GRID SELECT
------------------------------------------------------------------------

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x31,0x2e,0x39) -- lowest inactive bg
                  (0x31,0x2e,0x39) -- highest inactive bg
                  (0x61,0x57,0x72) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0xff,0xff,0xff) -- active fg

-- gridSelect menu layout
mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 30
    , gs_cellwidth    = 200
    , gs_cellpadding  = 8
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def

--------------------------------------------------------------------------------
-- | Customize layouts.
--

myOuterGaps :: l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Gaps l a
myOuterGaps = gaps [(U, 10), (D, 10), (L, 10), (R, 10)]

myLayouts = toggleLayouts (noBorders Full) others
  where
    others = emptyBSP ||| ResizableTall 1 (1.5/100) (toRational $ 2/(1+sqrt 5::Double)) [] ||| Grid ||| simpleTabbed

--------------------------------------------------------------------------------
-- | Customize the way 'XMonad.Prompt' looks and behaves.  It's a
-- great replacement for dzen.
myXPConfig :: XPConfig
myXPConfig = def
  { position          = Top
  , alwaysHighlight   = True
  , promptBorderWidth = 1
  , font              = myFont
  }



--------------------------------------------------------------------------------
-- | Manipulate windows as they are created.  The list given to
-- @composeOne@ is processed from top to bottom.  The first matching
-- rule wins.
--
-- Use the `xprop' tool to get the info you need for these matches.
-- For className, use the second value that xprop gives you.
myManageHook :: ManageHook
myManageHook = composeAll
  [ manageDocks
  , myWindowRules
  , manageHook desktopConfig
  ]

myWindowRules :: ManageHook
myWindowRules = composeOne
  [ className =? "Pidgin"      -?> doFloat
  , className =? "XCalc"       -?> doFloat
  , className =? "mpv"         -?> doFloat
  , className =? "vlc"         -?> doFloat
  , className =? "KeePass2"    -?> doFloat
  , className =? "KeePassXC"    -?> doFloat
  , className =? "MPCWindow"   -?> doCenterFloat
  , className =? "HTOPWindow"  -?> doCenterFloat
  , className =? "stalonetray" -?> doIgnore
  , isDialog                   -?> doCenterFloat
  -- Move transient windows to their parents
  , transience
  ]

-------------------------------------------------------------------------------
-- Additional key bindings / Keys definition
myKeyBindings :: [(String, X ())]
myKeyBindings =
        [ -- "Defaults" stolen from DistroTube
        ("M-S-q",   confirmPrompt myXPConfig "exit" (io exitSuccess))
        , ("M-p",     shellPrompt myXPConfig)
        , ("M-b",     sendMessage ToggleStruts) -- Make the windows go over status bars
        , ("M-f",     sendMessage ToggleLayout) -- Toggle between full screen and others
        , ("M-<Tab>", sendMessage NextLayout)
        , ("M-<Space>",     spawn myRofi)
        , ("M-r",     spawn myDMenu)
        , ("M-<Backspace>", focusUrgent)
        , ("M-S-c", kill)

        -- Xmonad
        , ("M-C-r", spawn "xmonad --recompile")
        , ("M-S-r", spawn "xmonad --restart")

        -- Windows navigation
        , ("M-j", windows W.focusDown)               -- Move focus to the next window
        , ("M-k", windows W.focusUp)                 -- Move focus to the prev window
        , ("M-S-m", windows W.swapMaster)            -- Swap the focused window and the master window
        , ("M-S-j", windows W.swapDown)              -- Swap the focused window with the next window
        , ("M-S-k", windows W.swapUp)                -- Swap the focused window with the prev window
        , ("M-S-<Backspace>", promote)                  -- Moves focused window to master, all others maintain order
        , ("M1-S-<Tab>", rotSlavesDown)              -- Rotate all windows except master and keep focus in place
        , ("M1-C-<Tab>", rotAllDown)                 -- Rotate all the windows in the current stack

        -- Gaps Control
        , ("M-C-g", sendMessage ToggleGaps)

        -- Lock screen
        , ("M-S-z",  spawn myScreenLock)

        -- Terminal apps
        , ("M-S-<Return>", spawn myTerminal)
        , ("M-i", spawn $ myTerminal ++ " --class HTOPWindow -e " ++ myConsoleInfo)
        , ("M-o", spawn $ myTerminal ++ " --class IRCWindow -e " ++ myConsoleIRCClient)

        --- Grid Select
        , ("M-S-t", spawnSelected'
          [ ("Audacity", "audacity")
          , ("Emacs", "emacs")
          , ("Firefox", "firefox")
          , ("Kitty", "kitty")
          , ("Keepass", "keepassxc")
          ])

        , ("M-S-g", goToSelected $ mygridConfig myColorizer)
        , ("M-S-b", bringSelected $ mygridConfig myColorizer)


        -- Brightness Control
        , ("<XF86MonBrightnessUp>", spawn "brightnessctl -d intel_backlight set 5%+")
        , ("<XF86MonBrightnessDown>", spawn "brightnessctl -d intel_backlight set 5%-")

        -- Media keys
        , ("<XF86AudioLowerVolume>", setMute True >> lowerVolume 4 >> return ())
        , ("<XF86AudioRaiseVolume>", setMute True >> raiseVolume 4 >> return ())
        , ("<XF86AudioMute>", Control.Monad.void toggleMute)
        , ("<XF86AudioPlay>", spawn "mpc -q toggle")
        , ("<XF86AudioPause>", spawn "mpc -q pause")
        , ("<XF86AudioStop>", spawn "mpc -q stop")
        , ("<XF86AudioPrev>", spawn "mpc -q prev")
        , ("<XF86AudioNext>", spawn "mpc -q next")
        , ("<XF86Music>", spawn $ myTerminal ++ " --class MPCWindow -e " ++ myConsoleMusicPlayer)
        , ("M-m", spawn $ myTerminal ++ " --class MPCWindow -e " ++ myConsoleMusicPlayer)
        ]

-------------------------------------------------------------------------------
-- Local programs
myTerminal :: String
myTerminal = "alacritty"
myModKey :: KeyMask
myModKey = mod4Mask -- Use the "Win" key for the mod key
myConsoleMusicPlayer :: String
myConsoleMusicPlayer = "ncmpcpp"
myConsoleInfo :: String
myConsoleInfo = "htop"
myConsoleIRCClient :: String
myConsoleIRCClient = "weechat"
-- myScreenCap :: String
-- myScreenCap = "spectacle"
myDMenu :: String
myDMenu = "dmenu_run -h 35"
myRofi :: String
myRofi = "rofi -combi-modi 'window,drun' -modi 'combi' -show combi"
myScreenLock :: String
myScreenLock = "xset s activate"
myFont :: String
myFont = "xft:Hack:style=Regular:size=12"
